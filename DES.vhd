LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY DES IS
    PORT(
       -- INPUTS
	   INPUT	: IN	UNSIGNED(7 DOWNTO 0);
	   LOAD		: IN	STD_LOGIC;
	   MODE		: IN	STD_LOGIC;	-- CODE OR DECODE
	   ISKEY	: IN	STD_LOGIC;	-- INPUT IS KEY 1, INPUT IS DATA 0
	   CLK		: IN	STD_LOGIC;
	   RESET	: IN	STD_LOGIC;
	   -- OUTPUTS
	   OUTPUT	: OUT	UNSIGNED(7 DOWNTO 0);
	   READYI	: OUT	STD_LOGIC;	-- I AM READY FOR INPUT DATA
	   READYO	: OUT	STD_LOGIC	-- OUTPUT IS READY
    );
END DES;

ARCHITECTURE BEHAVIORAL OF DES IS

	COMPONENT ROUND IS
		PORT(
			CLK		: IN	STD_LOGIC;
			MODE	: IN	STD_LOGIC;
			INPUT	: IN	UNSIGNED(63 DOWNTO 0);
			NEWKEY	: IN	STD_LOGIC;
			NEWTXT	: IN	STD_LOGIC;
			OUTPUT	: OUT	UNSIGNED(63 DOWNTO 0);
			VALID	: OUT	STD_LOGIC
		);
	END COMPONENT;

	COMPONENT IP IS
		GENERIC(
			DIFF	: INTEGER	:= 64
		);
		PORT(
			INPUT	: IN	UNSIGNED(63 DOWNTO 0);
			OUTPUT	: OUT	UNSIGNED(63 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT IP_1 IS
		GENERIC(
			DIFF	: INTEGER	:= 64
		);
		PORT(
			INPUT	: IN	UNSIGNED(63 DOWNTO 0);
			OUTPUT	: OUT	UNSIGNED(63 DOWNTO 0)
		);
	END COMPONENT;
	
	-- KEY-RELATED SIGNALS
	SIGNAL MAINKEY	: UNSIGNED(63 DOWNTO 0);
	SIGNAL NEWKEY	: STD_LOGIC;
	
	-- CODE AND DECODE-RELATED SIGNALS
	SIGNAL IN_REG	: UNSIGNED(63 DOWNTO 0);
	SIGNAL PLAINTXT	: UNSIGNED(63 DOWNTO 0);
	SIGNAL IPREG	: UNSIGNED(63 DOWNTO 0);
	SIGNAL RNDRSLT	: UNSIGNED(63 DOWNTO 0);
	SIGNAL TEMP0	: UNSIGNED(63 DOWNTO 0);
	SIGNAL TEMP1	: UNSIGNED(63 DOWNTO 0);
	SIGNAL OUTTMP	: UNSIGNED(63 DOWNTO 0);
	SIGNAL OUTTMP0	: UNSIGNED(55 DOWNTO 0);
	SIGNAL NEWTXT	: STD_LOGIC;
	SIGNAL RNDVALID	: STD_LOGIC;
	SIGNAL READYO1	: STD_LOGIC;
	SIGNAL MODETMP	: STD_LOGIC;
	SIGNAL OCNT		: INTEGER RANGE 0 TO 8;

BEGIN

	ROUND_INS:
	ROUND	PORT MAP(
		CLK, MODETMP, TEMP0, NEWKEY, NEWTXT, RNDRSLT, RNDVALID
	);

	IP_INS:
	IP	PORT MAP(
		PLAINTXT, IPREG
	);
	
	IP_1_INS:
	IP_1	PORT MAP(
		TEMP1, OUTTMP
	);
	
	TEMP0	<= IPREG	WHEN NEWKEY='0'	ELSE MAINKEY;
	
	PROCESS(CLK)
	BEGIN
	
		IF RISING_EDGE(CLK)	THEN
		
			READYO	<= '0';
			OCNT	<= 0;
			IF READYO1='1'	THEN
				OUTPUT		<= OUTTMP(63 DOWNTO 56);
				OUTTMP0		<= OUTTMP(55 DOWNTO 0);
				IF OCNT/=0	THEN
					OUTPUT	<= OUTTMP0(55 DOWNTO 48);
					OUTTMP0	<= OUTTMP0(47 DOWNTO 0) & "00000000";
				END IF;
				READYO	<= '1';
				OCNT	<= OCNT + 1;
				IF OCNT=7	THEN
					OCNT	<= 0;
					READYO1	<= '0';
					READYI	<= '1';
				END IF;
			END IF;
			
			IN_REG(7 DOWNTO 0)		<= INPUT;
			IN_REG(15 DOWNTO 8)		<= IN_REG(7 DOWNTO 0);
			IN_REG(23 DOWNTO 16)	<= IN_REG(15 DOWNTO 8);
			IN_REG(31 DOWNTO 24)	<= IN_REG(23 DOWNTO 16);
			IN_REG(39 DOWNTO 32)	<= IN_REG(31 DOWNTO 24);
			IN_REG(47 DOWNTO 40)	<= IN_REG(39 DOWNTO 32);
			IN_REG(55 DOWNTO 48)	<= IN_REG(47 DOWNTO 40);
			IN_REG(63 DOWNTO 56)	<= IN_REG(55 DOWNTO 48);
			
			NEWKEY	<= '0';
			NEWTXT	<= '0';
			IF LOAD='1'	THEN
				
				READYI	<= '1';
				IF ISKEY='1'	THEN
					NEWKEY		<= '1';
					MAINKEY		<= IN_REG;
				ELSE
					READYI		<= '0';
					NEWTXT		<= '1';
					MODETMP		<= MODE;
					PLAINTXT	<= IN_REG;
				END IF;
				
			END IF; -- END LOAD IF
		
			--TEMP1	<= (OTHERS=>'0');
			IF RNDVALID='1'	THEN
				TEMP1	<= RNDRSLT;
				READYO1	<= '1';
			END IF;
		
			IF RESET='1'	THEN
				MAINKEY		<= (OTHERS=>'0');
				NEWKEY		<= '0';
				IN_REG		<= (OTHERS=>'0');
				PLAINTXT	<= (OTHERS=>'0');
				TEMP1		<= (OTHERS=>'0');
				NEWTXT		<= '0';
				READYO1		<= '0';
				OCNT		<= 0;
				READYI		<= '1';
			END IF;
		
		END IF;
	
	END PROCESS;

END BEHAVIORAL;






